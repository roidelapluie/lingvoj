# Lingvoj

Linkvoy is a set of python script written to analyze the translation status of
the [FSFE](https://fsfe.org) website.

# Installation

Clone this repository, then install the dependencies:

```
cd linkvoy
virtualenv --python=python3 ve
source ve/bin/activate
pip install -r requirements.txt
```
# Usage

```
python3 linkvoj --repository-path=svn --output-dir=results --database=db.sqlite3
```

# Licenses

## Lingvoj

Lingvoj is released under the [GPL-3.0][gpl] or later.

[gpl]:https://www.gnu.org/licenses/gpl.txt


## Libraries

This softwares relies on the following python libraries:

* [docopt](https://github.com/docopt/docopt) (MIT)
* [python-dateutil](https://dateutil.readthedocs.org) (BSD-3-Clause)
* [https://github.com/pallets/markupsafe](MarkupSafe) (BSD)
* [http://jinja.pocoo.org/docs/dev/](Jinja2) (BSD)
* [six](https://pypi.python.org/pypi/six/) (MIT)
* [PySvn](https://github.com/dsoprea/PySvn) (GPL-2.0)
* [wheel](https://bitbucket.org/pypa/wheel) (MIT)

