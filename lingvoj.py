#!/usr/bin/env python3
"""lingvoj.py

Usage:
    lingvoj.py <repository> <database> <output>

Options:
    repository: Path to the SVN repository containing the FSFE Website. Must be created and update
        outside of this script.
    database: Path to the database file. If nonexisting, will be created.
    output: Path to the output directory. If nonexisting, will be created.
"""

from docopt import docopt

from lingvoj.run import Lingvoj

def main():
    arguments = docopt(__doc__, version='0.1')
    lingvoy_parameters = []
    lingvoy_parameters.append(arguments['<repository>'])
    lingvoy_parameters.append(arguments['<database>'])
    lingvoy_parameters.append(arguments['<output>'])
    exit(Lingvoj(*lingvoy_parameters).run())


if __name__ == '__main__':
    main()

