# Lingvoj Runner
import datetime
import logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

from dateutil.parser import parse

from lingvoj.svn import SvnRepository
from lingvoj.database import Database
from lingvoj.helpers import prepare_svn_diff
from lingvoj.output import Renderer

EXCLUDED_COMMITS = [20921]

class Lingvoj:
    def __init__(self, repository, database, outputdir):
        self.svn_repository = SvnRepository(repository)
        self.database = Database(database)
        self.renderer = Renderer(outputdir)

    def run(self):
        self.populate_revisions()
        self.populate_filelist()
        self.generate_output()


    def populate_revisions(self):
        logging.info('Populating SVN revisions in the database')
        svn_revisions = self.svn_repository.get_revisions()
        database_revisions = self.database.get_revisions()
        diff = prepare_svn_diff(svn_revisions, database_revisions + EXCLUDED_COMMITS, 500)
        for initial_revision, limit in diff:
            for revision in self.svn_repository.get_some_revisions(initial_revision, limit):
                self.database.insert_revision(revision.revision, revision.date)

    def populate_filelist(self):
        for path, revision in self.svn_repository.get_filelist():
            logging.debug('%s is at revision %s' % (path, revision))
            self.database.insert_file(path, revision)

    def get_languages(self):
        return sorted(self.database.extract_languages_from_db())

    def get_detailed_languages(self):
        tmp_result = {}
        result = []
        original = dict(self.database.extract_pages_from_db('en'))
        for language in self.get_languages():
            tmp_result[language] = {
                'name': language, 'link': '%s.html' % language,
                'outdated': 0, 'uptodate': 0, 'missing': 0}

            localpages = dict(self.database.extract_pages_from_db(language))
            for page, revision in original.items():
                local_name = '%s.%s.xhtml' % (page[:-9], language)
                if local_name in localpages.keys():
                    if localpages[local_name] >= original[page]:
                        tmp_result[language]['uptodate'] += 1
                    else:
                        tmp_result[language]['outdated'] += 1
                else:
                    tmp_result[language]['missing'] += 1

        for language in self.get_languages():
            result.append(tmp_result[language])
        return result

    def get_detailed_language(self, language):
        revisions = dict(self.database.get_revisions_with_date())
        result = {}
        result['name'] = language
        original = dict(self.database.extract_pages_from_db('en'))
        result['outdated'] = []
        result['uptodate'] = []
        result['missing'] = []

        localpages = dict(self.database.extract_pages_from_db(language))
        for page, revision in original.items():
            local_name = '%s.%s.xhtml' % (page[:-9], language)
            if local_name in localpages.keys():
                local_rev = localpages[local_name]
                if localpages[local_name] >= original[page]:
                    status = 'uptodate'
                else:
                    status = 'outdated'
            else:
                local_rev = 1
                status = 'missing'
            result[status].append({
                'orig_name': page,
                'orig_rev': revision,
                'orig_rev_date': parse(revisions[str(revision)]).strftime('%d-%m-%Y'),
                'local_name': local_name,
                'local_rev': local_rev,
                'local_rev_date': parse(revisions[str(local_rev)]).strftime('%d-%m-%Y'),
            })

        return result


    def generate_index(self, default_dict):
        detailed_languages = self.get_detailed_languages()

        self.renderer.create_page('index.html', 'index.html.j2', default_dict, {
            'languages': detailed_languages,
        })

    def generate_lang(self, lang, default_dict):
        detailed_language = self.get_detailed_language(lang)

        self.renderer.create_page('%s.html' % lang, 'language.html.j2', default_dict,
            detailed_language
        )


    def generate_output(self):
        # Generate Index.html
        default_dict = {'now': datetime.datetime.now()}

        languages = self.get_languages()
        default_dict['languages'] = languages
        default_dict['menu'] = [('index', 'index.html')]

        menu = [(k, '%s.html' % k) for k in sorted(languages)]
        default_dict['menu'] += menu

        self.generate_index(default_dict)

        for language in self.get_languages():
            self.generate_lang(language, default_dict)


