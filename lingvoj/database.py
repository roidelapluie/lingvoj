import sqlite3

class Database:
    def __init__(self, filename):
        self.database = sqlite3.connect(filename)
        self.create_tables()

    def create_tables(self):
        self.create_table('revisions', {
            'id': 'INTEGER PRIMARY KEY',
            'date': 'TIMESTAMP'
        })
        self.create_table('files', {
            'name': 'STRING PRIMARY KEY',
            'revision': 'INTEGER'
        })

    def create_table(self, name, fields):
        fields_for_sqlite = ','.join(['`%s` %s' % (k, v) for k, v in fields.items()])
        self.database.cursor().execute('CREATE TABLE IF NOT EXISTS `%s` (%s)' % (
            name, fields_for_sqlite))

    def revision_exists(self, revision):
        c = self.database.cursor()
        c.execute('SELECT count(*) FROM revisions WHERE `id` = %s' % revision)
        return c.fetchone() == (1,)

    def get_revisions(self):
        c = self.database.cursor()
        return [r[0] for r in list(c.execute('SELECT `id` from revisions'))]

    def get_revisions_with_date(self):
        c = self.database.cursor()
        return [(str(r[0]), r[1]) for r in list(c.execute('SELECT `id`, `date` from revisions'))]

    def insert_revision(self, revision, revision_date):
        c = self.database.cursor()
        c.execute('INSERT INTO revisions (id, date) VALUES(?,?)', (revision, revision_date))
        self.database.commit()

    def insert_file(self, filename, revision):
        c = self.database.cursor()
        c.execute('REPLACE INTO files (name, revision) VALUES(?,?)', (filename, revision))
        self.database.commit()

    def extract_languages_from_db(self):
        c = self.database.cursor()
        c.execute('SELECT name FROM files WHERE `name` LIKE "trunk/index.%.xhtml"')
        languages = [k[0].split('.')[1] for k in c.fetchall()]
        languages.remove('en')
        return languages

    def extract_pages_from_db(self, lang):
        c = self.database.cursor()
        c.execute('SELECT name, revision FROM files WHERE `name` LIKE "trunk/%%.%s.xhtml"' % lang)
        return c.fetchall()
