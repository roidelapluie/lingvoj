import os.path
import shutil

from jinja2 import Environment, PackageLoader

class Renderer:
    def __init__(self, path):
        if not os.path.isdir(path):
            os.mkdir(path)
        self.path = path
        self.env = Environment(loader=PackageLoader('lingvoj', 'renderer/templates'))

    def copy_asset(self, asset_name):
        dirname = os.path.dirname(os.path.realpath(__file__))
        src = os.path.join(dirname, 'renderer/assets', asset_name)
        dst = os.path.join(self.path, asset_name)
        shutil.copyfile(src, dst)


    def create_page(self, output_name, template_name, default_dict, page_dict={}):
        template = self.env.get_template(template_name)
        with open(os.path.join(self.path, output_name), 'w') as f:
            f.write(template.render(default_dict=default_dict, **page_dict))

