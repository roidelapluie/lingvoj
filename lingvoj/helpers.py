def prepare_svn_diff(list1, list2, maxfetch):
    '''
    Returns a tuple of (first_revision, limit) suitable for svn, to fetch missing revisions.
    '''
    result = []
    initial_commit = None
    for commit in list1:
        if not commit in list2:
            if initial_commit is None:
                initial_commit = commit
                previous = commit - 1
                i = 0
            if commit == previous + 1:
                previous = commit
                i+=1
            else:
                result.append((initial_commit, i))
                initial_commit = commit
                previous = commit
                i = 1
            if i == maxfetch:
                result.append((initial_commit, i))
                initial_commit = None
                previous = None
    if not initial_commit is None:
        result.append((initial_commit, i))
    return result


