import svn.local
import os.path

class SvnRepository:
    def __init__(self, path):
        self.client = svn.local.LocalClient(path)
        self.info = self.client.info()

    def get_last_revision(self):
        return self.info['commit_revision']

    def get_revisions(self):
        return list(range(1, int(self.get_last_revision())+1))

    def get_revision(self, revision):
        return self.client.log_default(revision_from=revision, revision_to=revision)

    def get_revision_date(self, revision):
        rev = next(self.get_revision(revision))
        return rev.date

    def get_some_revisions(self, initial_revision, limit):
        return self.client.log_default(revision_from=initial_revision, limit=limit)

    def get_filelist(self):
        entries = self.client.list_recursive(rel_path='trunk')
        for rel_path, entry in entries:
            yield (os.path.join(rel_path, entry['name']), entry['commit_revision'])
