import unittest
from lingvoj.helpers import prepare_svn_diff

class TestPrepareSvnDiff(unittest.TestCase):
    def test_same_lists(self):
        self.assertEqual(prepare_svn_diff([1,2,3], [1,2,3], 10), [])

    def test_simple_example(self):
        self.assertEqual(prepare_svn_diff([1], [], 10), [(1,1)])

    def test_three_commits_example(self):
        self.assertEqual(prepare_svn_diff([1,2,3], [], 10), [(1,3)])

    def test_ten_commits_example(self):
        self.assertEqual(prepare_svn_diff([1,2,3,4,5,6,7,8,9,10], [], 5), [(1,5),(6,5)])

    def test_ten_non_following_commits_example(self):
        self.assertEqual(prepare_svn_diff([1,2,4,5,6,7,8,9,10,11], [], 5), [
            (1,2),
            (4,5),
            (9,3),
        ])

    def test_asymetrical_commits_example(self):
        self.assertEqual(prepare_svn_diff([1,2,4,5,6,7,8,9,10,11], [5,6], 5), [
            (1,2),
            (4,1),
            (7,5),
        ])


if __name__ == '__main__':
    unittest.main()
